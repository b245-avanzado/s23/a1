console.log("Pokemon Battle!");

// Initialization of trainer Start
let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemons: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function(){
            console.log("Pikachu! I choose you!");
        }
}

console.log(trainer);
// Initialization of trainer End

// Accessing using dot notation Start
console.log("Result of dot notation: ");
console.log(trainer.name);
// Accessing using dot notation End

// Accessing using bracket notation Start
console.log("Result of square bracket notation: ");
console.log(trainer["pokemons"]);
// Accessing using bracket notation End

// Invocation of "talk" method Start
console.log("Result of talk method: ")
trainer.talk();
// Invocation "talk" method End

// Constructor function for creating a pokemon Start
function Pokemon(name, level){
    this.pokemonName = name;
    this.pokemonLevel = level;
    this.pokemonHealth = 2 * level;
    this.pokemonAttack = level;

    this.tackle = function(targetPokemon){
        console.log(this.pokemonName + " tackled " + targetPokemon.pokemonName);
        let hpAfterTackle = targetPokemon.pokemonHealth - this.pokemonAttack;
            console.log(targetPokemon.pokemonName + " health is now reduced to " + hpAfterTackle);
        if (hpAfterTackle <= 0) {
            targetPokemon.faint(targetPokemon);
        }
    }

    this.faint = function(targetPokemon){
        console.log(targetPokemon.pokemonName + " fainted!")
    }
}
// Constructor function for creating a pokemon End

// Instantiation of pokemons Start
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);
// Instantiation of pokemons End

// Invocation of tackle method Start
geodude.tackle(pikachu);
mewtwo.tackle(geodude);
// Invocation of tackle method End